import Foundation

protocol CustomDecodableFromData {
  init(customDecodable: Data) throws
}
