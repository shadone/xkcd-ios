import Foundation

extension Model {
  struct SearchResult: CustomDecodableFromData {
    var results: [Model.ComicId]

    init(results: [Model.ComicId]) {
      self.results = results
    }

    init(customDecodable: Data) throws {
      // this is the format the data is coming in:
      //0.0498
      //0
      //55 /wiki/images/a/ac/useless.jpg
      //68 /wiki/images/e/e2/five_thirty.jpg
      // ...

      let lines = String(data: customDecodable, encoding: .utf8)?.split(separator: "\n")
      // skip the first two lines
      self.results =
        lines?.dropFirst(2).compactMap { line in
          return line.split(separator: " ").first
        }
        .map(String.init)
        .compactMap(Int.init) ?? []
    }
  }
}
