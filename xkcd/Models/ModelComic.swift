import Foundation

extension Model {
  typealias ComicId = Int // TODO: make type safe

  struct Comic: Decodable {
    let id: ComicId
    let title: String
    let alt: String
    let imageUrl: String

    enum CodingKeys: String, CodingKey {
      case id = "num"
      case alt
      case imageUrl = "img"
      case title
    }
  }
}
