import Foundation

enum APIError: Error {
  case invalidServiceConfiguration
  case networkUnavailable
  case badResponse(httpCode: Int)
  case invalidResponse(String)
}
