import Foundation
import ReactiveSwift

class APIServiceWithStaticMockData: APIServiceType {
  let comics: [Model.Comic] = [
    Model.Comic(id: 4, title: "Hello4", alt: "alt hello4", imageUrl: "https://imgs.xkcd.com/comics/stack.png"),
    Model.Comic(id: 3, title: "Hello3", alt: "alt hello3", imageUrl: "https://imgs.xkcd.com/comics/millennials.png"),
    Model.Comic(id: 2, title: "Hello2", alt: "alt hello2", imageUrl: "https://imgs.xkcd.com/comics/stack.png"),
    Model.Comic(id: 1, title: "Hello1", alt: "alt hello1", imageUrl: "https://imgs.xkcd.com/comics/stack.png"),
  ]

  func baseUrl(for: APIServer) -> URL? {
    return URL(string: "https://example.com")
  }

  func fetchLatestComic() -> SignalProducer<Model.Comic, APIError> {
    return .init(value: comics[0])
  }

  func fetchComic(id: Int) -> SignalProducer<Model.Comic, APIError> {
    guard let comic = comics.first(where: { $0.id == id }) else {
      return .init(error: .badResponse(httpCode: 404))
    }
    return .init(value: comic)
  }

  func search(term: String) -> SignalProducer<Model.SearchResult, APIError> {
    return SignalProducer(value: Model.SearchResult(results: [
      comics[1].id,
      comics[3].id,
    ]))
      .delay(1, on: QueueScheduler())
  }


  func shareUrl(for comicId: Model.ComicId) -> URL {
    let url = URL(string: "\(comicId)/", relativeTo: baseUrl(for: .xkcd))!
    // WAT. URLs constructed from relative strings are not supported by NSItemProvider
    // rdar://46030386
    return URL(string: url.absoluteString)!
  }
}
