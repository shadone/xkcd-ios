import Foundation
import ReactiveSwift

protocol APIServiceType {
  func baseUrl(for: APIServer) -> URL?

  func fetchLatestComic() -> SignalProducer<Model.Comic, APIError>
  func fetchComic(id: Int) -> SignalProducer<Model.Comic, APIError>

  func search(term: String) -> SignalProducer<Model.SearchResult, APIError>

  func shareUrl(for comicId: Model.ComicId) -> URL
}
