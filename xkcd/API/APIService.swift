import Foundation
import ReactiveSwift

enum APIServer {
  case xkcd
  case search
}

struct APIEndpointParams {
  let server: APIServer
  let path: String
  let query: [String: String]?

  init(_ server: APIServer, _ path: String, query: [String: String]? = nil) {
    self.server = server
    self.path = path
    self.query = query
  }
}

enum APIEndpoint {
  case fetchLatestComic
  case fetchComic(id: Int)
  case search(term: String)

  var params: APIEndpointParams {
    switch self {
    case .fetchLatestComic:
      return .init(.xkcd, "/info.0.json")
    case let .fetchComic(id):
      return .init(.xkcd, "\(id)/info.0.json")
    case let .search(term):
      return .init(.search, "/process", query: ["action": "xkcd", "query": term])
    }
  }

  func url(relativeTo baseUrl: URL) -> URL? {
    guard let url = URL(string: self.params.path, relativeTo: baseUrl) else {
      return nil
    }

    var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
    components?.queryItems = params.query?.compactMap { item in
      URLQueryItem(name: item.key, value: item.value)
    }
    return components?.url
  }
}

class APIService: APIServiceType {
  let servers: [APIServer: URL]

  let urlCache: URLCache = {
    let cache = URLCache(memoryCapacity: 5.megabytes, diskCapacity: 50.megabytes, diskPath: nil)
    return cache
  }()

  lazy var urlSession: URLSession = {
    let config = URLSessionConfiguration.default
    config.urlCache = urlCache
    let session = URLSession(configuration: config)
    return session
  }()

  init(xkcd: String = "https://xkcd.com", search: String = "https://relevantxkcd.appspot.com") {
    servers = {
      var servers: [APIServer: URL] = [:]
      if let url = URL(string: xkcd) {
        servers[.xkcd] = url
      }
      if let url = URL(string: search) {
        servers[.search] = url
      }
      return servers
    }()
  }

  func baseUrl(for type: APIServer) -> URL? {
    return servers[type]
  }

  func fetchLatestComic() -> SignalProducer<Model.Comic, APIError> {
    return request(.fetchLatestComic)
  }

  func fetchComic(id: Int) -> SignalProducer<Model.Comic, APIError> {
    return request(.fetchComic(id: id))
  }

  func search(term: String) -> SignalProducer<Model.SearchResult, APIError> {
    return request(.search(term: term))
  }

  func shareUrl(for comicId: Model.ComicId) -> URL {
    guard
      let baseUrl = baseUrl(for: .xkcd),
      let url = URL(string: "\(comicId)/", relativeTo: baseUrl)
    else {
      return URL(string: "https://mydomain/report/share/\(comicId)")!
    }
    // WAT. URLs constructed from relative strings are not supported by NSItemProvider
    // rdar://46030386
    return URL(string: url.absoluteString)!
  }

  // MARK: - Helper functions

  /// Decode a model from a `json` dictionary.
  /// - parameter json: dictionary for model to parse.
  func decode<M: Decodable>(_ data: Data) -> SignalProducer<M, APIError> {
    return SignalProducer(value: data)
      .flatMap(.concat) { data -> SignalProducer<M, APIError> in
        do {
          let nonEmptyData = data.count != 0 ? data : "{}".data(using: .utf8)!
          let decoder = JSONDecoder()
          let model = try decoder.decode(M.self, from: nonEmptyData)
          return .init(value: model)
        } catch {
          return .init(error: .invalidResponse(error.localizedDescription))
        }
    }
  }

  /// Make network request for `endpoint`.
  /// - parameter endpoint: the endpoint we are making request to, describing all parameters for the api request.
  func request<M: Decodable>(_ endpoint: APIEndpoint) -> SignalProducer<M, APIError> {
    guard
      let baseUrl = baseUrl(for: endpoint.params.server),
      let url = endpoint.url(relativeTo: baseUrl)
    else {
      return .init(error: .invalidServiceConfiguration)
    }

    let request = URLRequest(url: url)

    return URLSession.shared.fetchData(request)
      .flatMap(.concat, decode)
      .logError(identifier: endpoint.params.path)
  }

  func decode<M: CustomDecodableFromData>(_ data: Data) -> SignalProducer<M, APIError> {
    do {
      let model = try M(customDecodable: data)
      return .init(value: model)
    } catch {
      return .init(error: .invalidResponse(error.localizedDescription))
    }
  }

  func request<M: CustomDecodableFromData>(_ endpoint: APIEndpoint) -> SignalProducer<M, APIError> {
    guard
      let baseUrl = baseUrl(for: endpoint.params.server),
      let url = endpoint.url(relativeTo: baseUrl)
      else {
        return .init(error: .invalidServiceConfiguration)
    }

    let request = URLRequest(url: url)

    return URLSession.shared.fetchData(request)
      .flatMap(.concat, decode)
      .logError(identifier: endpoint.params.path)
  }
}
