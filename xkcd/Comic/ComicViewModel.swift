import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result

protocol ComicViewModelInputs {
  func viewWillAppear(_ animated: Bool)
  func configureWith(currentComicId: Model.ComicId, comicIds: [Model.ComicId])
  func share(comicId: Model.ComicId)
}

protocol ComicViewModelOutputs {
  typealias LoadedData = (current: (index: Int, comic: Model.Comic), comicIds: [Model.ComicId])
  var loaded: Signal<LoadedData, NoError> { get }
  var goToShare: Signal<Model.Comic, NoError> { get }
}

protocol ComicViewModelType {
  var inputs: ComicViewModelInputs { get }
  var outputs: ComicViewModelOutputs { get }
}

class ComicViewModel:
  ComicViewModelType, ComicViewModelInputs, ComicViewModelOutputs {

  init() {
    loaded = configureProperty.signal
      .takeWhen(viewWillAppearProperty.signal)
      .skipNil()
      .flatMap(.concat) { data -> SignalProducer<LoadedData, NoError> in
        let index = data.comicIds.firstIndex(of: data.currentComicId)
        assert(index != nil)
        return Application.current.apiService.fetchComic(id: data.currentComicId)
          .demoteErrors()
          .map { comic in
            return (current: (index: index ?? 0, comic: comic), comicIds: data.comicIds)
        }
      }

    goToShare = shareProperty.signal
      .skipNil()
      .flatMap(.concat) { comicId in
        Application.current.apiService.fetchComic(id: comicId)
          .demoteErrors()
    }
  }

  // MARK: ComicViewModelOutputs conformance
  var loaded: Signal<LoadedData, NoError>
  var goToShare: Signal<Model.Comic, NoError>

  // MARK: ComicViewModelInputs conformance

  private let viewWillAppearProperty = MutableProperty(false)
  func viewWillAppear(_ animated: Bool) {
    viewWillAppearProperty.value = animated
  }

  private let configureProperty: MutableProperty<(currentComicId: Model.ComicId, comicIds: [Model.ComicId])?> = MutableProperty(nil)
  func configureWith(currentComicId: Model.ComicId, comicIds: [Model.ComicId]) {
    configureProperty.value = (currentComicId: currentComicId, comicIds: comicIds)
  }

  private let shareProperty: MutableProperty<Model.ComicId?> = MutableProperty(nil)
  func share(comicId: Model.ComicId) {
    shareProperty.value = comicId
  }

  // MARK: ComicViewModelType conformance

  var inputs: ComicViewModelInputs { return self }
  var outputs: ComicViewModelOutputs { return self }
}
