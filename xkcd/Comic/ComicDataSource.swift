import UIKit

class ComicDataSource: ValueCellDataSource {
  func set(_ values: [Model.ComicId]) {
    self.set(values, cell: ComicViewCell.self)
  }

  func set(_ value: Model.Comic, row: Int) {
    self.set(value, cell: ComicViewCell.self, row: row)
  }

  override func configure(cell: UICollectionViewCell, with value: Any) {
    switch (cell, value) {
    case let (cell as ComicViewCell, value as Model.ComicId):
      cell.configure(with: value)
    default:
      preconditionFailure()
    }
  }
}
