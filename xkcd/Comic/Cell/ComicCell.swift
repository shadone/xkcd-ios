import UIKit
import ReactiveSwift
import ReactiveCocoa

class ComicViewCell: UICollectionViewCell, ValueCell {
  typealias Value = Model.ComicId
  static let cellIdentifier = "ComicViewCell"

  let viewModel: ComicCellViewModelType = ComicCellViewModel()

  var titleLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.font = UIFont.preferredFont(forTextStyle: .title1)
    label.numberOfLines = 0
    label.setContentHuggingPriority(.required, for: .vertical)
    return label
  }()

  var descriptionLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.numberOfLines = 0
    label.textAlignment = .justified
    label.font = UIFont.preferredFont(forTextStyle: .footnote)
    label.setContentHuggingPriority(.required, for: .vertical)
    return label
  }()

  var imageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setup() {
    [
      titleLabel,
      imageView,
      descriptionLabel,
    ].forEach(contentView.addSubview(_:))

    titleLabel.snp.makeConstraints {
      $0.top.equalToSuperview()
      $0.leading.equalToSuperview().offset(8)
      $0.trailing.equalToSuperview().inset(8)
    }

    imageView.snp.makeConstraints {
      $0.top.equalTo(titleLabel.snp.bottom)
      $0.leading.equalToSuperview()
      $0.trailing.equalToSuperview()
    }

    descriptionLabel.snp.makeConstraints {
      $0.top.equalTo(imageView.snp.bottom)
      $0.leading.equalToSuperview().offset(8)
      $0.trailing.equalToSuperview().inset(8)
      $0.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottom)
    }

    bindViewModel()
  }

  func bindViewModel() {
    titleLabel.reactive.text <~ viewModel.outputs.title
    imageView.reactive.image <~ viewModel.outputs.image
    descriptionLabel.reactive.attributedText <~ viewModel.outputs.description
  }

  func configure(with value: Model.ComicId) {
    viewModel.inputs.configure(comicId: value)
  }
}
