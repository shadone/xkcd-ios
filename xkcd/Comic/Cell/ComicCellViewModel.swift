import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result

protocol ComicCellViewModelInputs {
  func prepareForReuse()
  func configure(comicId: Model.ComicId)
}

protocol ComicCellViewModelOutputs {
  var title: Signal<String, NoError> { get }
  var description: Signal<NSAttributedString, NoError> { get }
  var imageUrl: Signal<URL, NoError> { get }
  var image: Signal<UIImage, NoError> { get }
}

protocol ComicCellViewModelType {
  var inputs: ComicCellViewModelInputs { get }
  var outputs: ComicCellViewModelOutputs { get }
}

class ComicCellViewModel:
  ComicCellViewModelType,
  ComicCellViewModelInputs,
  ComicCellViewModelOutputs {

  init() {
    title = Signal.merge([
      value.signal.skipNil().map(\.title),
      prepareForReuseProperty.signal.map(value: "")
    ])

    description = Signal.merge([
      value.signal.skipNil().map { comic in
        let comicNumber = NSAttributedString(string: "#\(comic.id)", attributes: [
          NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline),
        ])
        let altText = NSAttributedString(string: comic.alt, attributes: [
          NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote),
        ])

        let result = NSMutableAttributedString()
        [
          comicNumber,
          NSAttributedString(string: "\n"),
          altText,
        ].forEach(result.append)
        return result
      },
      prepareForReuseProperty.signal.map(value: NSAttributedString())
    ])

    imageUrl = value.signal.skipNil().map(\.imageUrl).map { imageUrl in
      return URL(string: imageUrl)!
    }

    let image = imageUrl.flatMap(.concat, { url -> SignalProducer<UIImage, NoError> in
      return Application.current.imageService.get(url)
    })

    self.image = Signal.merge([
      image,
      prepareForReuseProperty.signal.map(value: UIImage()) // TODO: placeholder image?
    ])

    prepareForReuseProperty.signal
      .observeValues { [weak self] _ in
        self?.value.value = nil
    }

    configureProperty.signal
      .skipNil()
      .flatMap(.concat) { comicId -> SignalProducer<Model.Comic, NoError> in
        return Application.current.apiService.fetchComic(id: comicId)
          .demoteErrors()
          .take(until: self.prepareForReuseProperty.signal)
      }
      .observeValues { [weak self] comic in
        self?.value.value = comic
    }
  }

  var value: MutableProperty<Model.Comic?> = MutableProperty(nil)

  // MARK: ComicCellViewModelOutputs conformance

  let title: Signal<String, NoError>
  let description: Signal<NSAttributedString, NoError>
  let imageUrl: Signal<URL, NoError>
  let image: Signal<UIImage, NoError>

  // MARK: ComicCellViewModelInputs conformance

  private let prepareForReuseProperty: MutableProperty<Void> = MutableProperty(())
  func prepareForReuse() {
    prepareForReuseProperty.value = ()
  }

  private let configureProperty: MutableProperty<Model.ComicId?> = MutableProperty(nil)
  func configure(comicId: Model.ComicId) {
    configureProperty.value = comicId
  }

  // MARK: ViewModelType conformance

  var inputs: ComicCellViewModelInputs { return self }
  var outputs: ComicCellViewModelOutputs { return self }
}

