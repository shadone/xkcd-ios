import UIKit
import ReactiveSwift

class ComicViewController: UIViewController {
  let viewModel: ComicViewModelType = ComicViewModel()
  let dataSource = ComicDataSource()

  let collectionViewLayout = ComicViewCollectionViewLayout()

  lazy var collectionView: UICollectionView = {
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
    collectionView.backgroundColor = .white
    collectionView.delegate = self
    collectionView.dataSource = dataSource
    collectionView.alwaysBounceHorizontal = true
    collectionView.isPagingEnabled = true
    collectionView.contentInsetAdjustmentBehavior = .never
    collectionView.register(ComicViewCell.self, forCellWithReuseIdentifier: ComicViewCell.cellIdentifier)
    return collectionView
  }()

  static func configuredWith(currentComicId: Model.ComicId, comicIds: [Model.ComicId]) -> ComicViewController {
    let comicVC = ComicViewController()
    comicVC.viewModel.inputs.configureWith(currentComicId: currentComicId, comicIds: comicIds)
    return comicVC
  }

  init() {
    super.init(nibName: nil, bundle: nil)
    bindViewModel()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white
    navigationItem.largeTitleDisplayMode = .never
    navigationItem.rightBarButtonItems = [
      UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareButtonTapped(_:)))
    ]

    view.addSubview(collectionView)
    collectionView.snp.makeConstraints {
      $0.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
      $0.leading.equalToSuperview()
      $0.trailing.equalToSuperview()
      $0.bottom.equalToSuperview()
    }
  }

  func bindViewModel() {
    viewModel.outputs.loaded
      .observe(on: UIScheduler())
      .observeValues { [weak self] data in
        self?.dataSource.set(data.comicIds)
        self?.collectionView.reloadData()
        // had to async here :( scrollToItem doesn't work immediately after reloadData
        // probably because the whole viewcontroller/view hadn't layout yet.
        DispatchQueue.main.async {
          self?.collectionView.scrollToItem(at: IndexPath(row: data.current.index, section: 0),
                                            at: .left, animated: false)
        }
    }

    viewModel.outputs.goToShare
      .observe(on: UIScheduler())
      .observeValues { [weak self] comic in
        self?.goTo(share: comic)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.inputs.viewWillAppear(animated)
  }

  @objc func shareButtonTapped(_ sender: UIBarButtonItem) {
    guard let currentIndexPath = collectionView.indexPathsForVisibleItems.first else {
      assertionFailure()
      return
    }

    let value = dataSource[currentIndexPath]

    switch value {
    case let value as Model.ComicId:
      viewModel.inputs.share(comicId: value)
    default:
      preconditionFailure()
    }
  }

  func goTo(share comic: Model.Comic) {
    let shareVC = ComicShareSheetViewController.configuredWith(comic: comic)
    self.present(shareVC, animated: true)
  }
}

extension ComicViewController: UICollectionViewDelegate {
}
