import UIKit

class ComicViewCollectionViewLayout: UICollectionViewFlowLayout {
  override init() {
    super.init()
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setup() {
    scrollDirection = .horizontal
    minimumLineSpacing = 0
    minimumInteritemSpacing = 0
  }

  override func prepare() {
    if let collectionView = collectionView {
      itemSize = collectionView.bounds.size
    }
    super.prepare()
  }

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    itemSize = newBounds.size
    return true
  }
}
