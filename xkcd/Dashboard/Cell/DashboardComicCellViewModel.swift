import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result

//enum Either<M, N> {
//  case left(M)
//  case right(N)
//
//  var leftValue: M? {
//    switch self {
//    case let .left(value): return value
//    case .right: return nil
//    }
//  }
//
//  var rightValue: N? {
//    switch self {
//    case .left: return nil
//    case let .right(value): return value
//    }
//  }
//}

protocol DashboardComicCellViewModelInputs {
  func prepareForReuse()
  func configure(comicId: Model.ComicId)
}

protocol DashboardComicCellViewModelOutputs {
  var title: Signal<String, NoError> { get }
  var imageUrl: Signal<URL, NoError> { get }
  var image: Signal<UIImage, NoError> { get }
}

protocol DashboardComicCellViewModelType {
  var inputs: DashboardComicCellViewModelInputs { get }
  var outputs: DashboardComicCellViewModelOutputs { get }
}

class DashboardComicCellViewModel:
  DashboardComicCellViewModelType,
  DashboardComicCellViewModelInputs,
  DashboardComicCellViewModelOutputs {

  init() {
    title = Signal.merge([
      value.signal.skipNil().map(\.title),
      prepareForReuseProperty.signal.map(value: "")
    ])

    imageUrl = value.signal.skipNil().map(\.imageUrl).map { imageUrl in
      return URL(string: imageUrl)!
    }

    let prepareForReuseSignal = self.prepareForReuseProperty.signal
    let image = imageUrl.flatMap(.concat, { url -> SignalProducer<UIImage, NoError> in
      return Application.current.imageService.get(url)
        .take(until: prepareForReuseSignal)
    })

    self.image = Signal.merge([
      image,
      prepareForReuseProperty.signal.map(value: UIImage()) // TODO: placeholder image?
    ])

    prepareForReuseProperty.signal
      .observeValues { [weak self] _ in
        self?.value.value = nil
    }

    configureProperty.signal
      .skipNil()
      .flatMap(.concat) { comicId -> SignalProducer<Model.Comic, NoError> in
        return Application.current.apiService.fetchComic(id: comicId)
          .demoteErrors()
          .take(until: self.prepareForReuseProperty.signal)
      }
      .observeValues { [weak self] comic in
        self?.value.value = comic
    }
  }

  var value: MutableProperty<Model.Comic?> = MutableProperty(nil)

  // MARK: DashboardComicCellViewModelOutputs conformance

  let title: Signal<String, NoError>
  let imageUrl: Signal<URL, NoError>
  let image: Signal<UIImage, NoError>

  // MARK: DashboardComicCellViewModelInputs conformance

  private let prepareForReuseProperty: MutableProperty<Void> = MutableProperty(())
  func prepareForReuse() {
    prepareForReuseProperty.value = ()
  }

  private let configureProperty: MutableProperty<Model.ComicId?> = MutableProperty(nil)
  func configure(comicId: Model.ComicId) {
    configureProperty.value = comicId
  }

  // MARK: DashboardViewModelType conformance

  var inputs: DashboardComicCellViewModelInputs { return self }
  var outputs: DashboardComicCellViewModelOutputs { return self }
}

