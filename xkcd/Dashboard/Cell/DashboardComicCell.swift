import UIKit
import SnapKit
import ReactiveSwift
import ReactiveCocoa

class DashboardComicCell: UICollectionViewCell, ValueCell {
  typealias Value = Model.ComicId
  static let cellIdentifier = "DashboardComicCell"

  let viewModel: DashboardComicCellViewModelType = DashboardComicCellViewModel()

  var titleLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.textAlignment = .center
    label.font = UIFont.preferredFont(forTextStyle: .title3)
    return label
  }()

  var imageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setup() {
    [
      titleLabel,
      imageView,
    ].forEach(contentView.addSubview(_:))

    titleLabel.snp.makeConstraints {
      $0.leading.equalToSuperview()
      $0.trailing.equalToSuperview()
      $0.top.equalToSuperview()
    }

    imageView.snp.makeConstraints {
      $0.leading.equalToSuperview()
      $0.trailing.equalToSuperview()
      $0.top.equalTo(titleLabel.snp.bottom).offset(-3)
      $0.bottom.equalToSuperview()
    }

    bindViewModel()
  }

  func bindViewModel() {
    titleLabel.reactive.text <~ viewModel.outputs.title
    imageView.reactive.image <~ viewModel.outputs.image
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    viewModel.inputs.prepareForReuse()
  }

  func configure(with value: Model.ComicId) {
    viewModel.inputs.configure(comicId: value)
  }
}
