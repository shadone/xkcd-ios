import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result

protocol DashboardViewModelInputs {
  func viewWillAppear(_ animated: Bool)
  func viewDidLoad()
  func tappedOnComic(id: Model.ComicId)
  func search(term: String)
  func searchCancelled()
}

protocol DashboardViewModelOutputs {
  var goToComic: Signal<(current: Model.ComicId, comicIds: [Model.ComicId]), NoError> { get }
  var comicIds: Signal<[Model.ComicId], NoError> { get }
  var isSearching: Signal<Bool, NoError> { get }
}

protocol DashboardViewModelType {
  var inputs: DashboardViewModelInputs { get }
  var outputs: DashboardViewModelOutputs { get }
}

class DashboardViewModel:
  DashboardViewModelType, DashboardViewModelInputs, DashboardViewModelOutputs {

  init() {
    let fetchAllComicIds = Application.current.apiService.fetchLatestComic()
      .map { comic -> [Model.ComicId] in
        let latestComicId = comic.id
        let comicIds = (1...latestComicId).reversed()
        return Array(comicIds)
      }

    let comicIdsAfterViewDidLoad = viewDidLoadProperty.signal
      .flatMap(.concat, { _ in fetchAllComicIds.demoteErrors() })

    goToComic = Signal.combineLatest(
      tappedOnComicProperty.signal.skipNil(),
      comicIdsAfterViewDidLoad
    ).map { (current: $0.0, comicIds: $0.1) }

    let restoreAfterSearch = searchProperty.signal
      .withLatest(from: fetchAllComicIds.demoteErrors())
      .map { value -> [Model.ComicId]? in
        let term = value.0
        if term.isEmpty {
          let allComicIds = value.1
          return allComicIds
        }
        return nil
      }
      .skipNil()

    let triggerSearch = searchProperty.signal
      .filter { !$0.isEmpty }
      .skipRepeats()
      .throttle(0.5, on: QueueScheduler.main)

    let searchCancelledSignal = searchCancelledProperty.signal

    let searchResultsFromNetwork = triggerSearch
      .flatMap(.concat) { term -> SignalProducer<[Model.ComicId], NoError> in
        return Application.current.apiService.search(term: term)
          .demoteErrors()
          .map { $0.results }
          .take(until: searchCancelledSignal)
      }

    isSearching = Signal.merge(
      triggerSearch.map(value: true),
      restoreAfterSearch.map(value: false),
      searchResultsFromNetwork.map(value: false),
      searchCancelledProperty.signal.map(value: false)
    ).skipRepeats()

    comicIds = Signal.merge(
      comicIdsAfterViewDidLoad,
      restoreAfterSearch,
      searchResultsFromNetwork
    )
  }

  // MARK: DashboardViewModelOutputs conformance
  var goToComic: Signal<(current: Model.ComicId, comicIds: [Model.ComicId]), NoError>
  var comicIds: Signal<[Model.ComicId], NoError>
  var isSearching: Signal<Bool, NoError>

  // MARK: DashboardViewModelInputs conformance

  private let viewWillAppearProperty = MutableProperty(false)
  func viewWillAppear(_ animated: Bool) {
    viewWillAppearProperty.value = animated
  }

  private let viewDidLoadProperty: MutableProperty<Void> = MutableProperty(())
  func viewDidLoad() {
    viewDidLoadProperty.value = ()
  }

  private let tappedOnComicProperty: MutableProperty<Model.ComicId?> = MutableProperty(nil)
  func tappedOnComic(id: Model.ComicId) {
    tappedOnComicProperty.value = id
  }

  private let searchProperty: MutableProperty<String> = MutableProperty("")
  func search(term: String) {
    searchProperty.value = term
  }

  private let searchCancelledProperty: MutableProperty<Void> = MutableProperty(())
  func searchCancelled() {
    searchCancelledProperty.value = ()
  }

  // MARK: DashboardViewModelType conformance

  var inputs: DashboardViewModelInputs { return self }
  var outputs: DashboardViewModelOutputs { return self }
}
