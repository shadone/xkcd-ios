import UIKit

class DashboardCollectionViewLayout: UICollectionViewFlowLayout {
  override init() {
    super.init()
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setup() {
    scrollDirection = .vertical
    minimumLineSpacing = 10
    minimumInteritemSpacing = 10
    sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  }

  func itemSize(for bounds: CGSize) -> CGSize {
    let width = (bounds.width - minimumInteritemSpacing - sectionInset.left - sectionInset.right) / 2
    return CGSize(width: width, height: 100)
  }

  override func prepare() {
    if let collectionView = collectionView {
      self.itemSize = itemSize(for: collectionView.bounds.size)
    }
    super.prepare()
  }

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    let newItemSize = itemSize(for: newBounds.size)
    if itemSize != newItemSize {
      return true
    }
    return super.shouldInvalidateLayout(forBoundsChange: newBounds)
  }
}
