import UIKit

class DashboardDataSource: ValueCellDataSource {
  func set(_ values: [Model.ComicId]) {
    self.set(values, cell: DashboardComicCell.self)
  }

  func set(_ value: Model.Comic, row: Int) {
    self.set(value, cell: DashboardComicCell.self, row: row)
  }

  override func configure(cell: UICollectionViewCell, with value: Any) {
    switch (cell, value) {
    case let (cell as DashboardComicCell, value as Model.ComicId):
      cell.configure(with: value)
    default:
      preconditionFailure()
    }
  }
}
