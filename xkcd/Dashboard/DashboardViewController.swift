import UIKit
import ReactiveSwift

class DashboardViewController: UIViewController {
  let viewModel: DashboardViewModelType = DashboardViewModel()
  let dataSource = DashboardDataSource()

  lazy var searchController: UISearchController = {
    let searchController = UISearchController(searchResultsController: nil)
    searchController.delegate = self
    searchController.searchResultsUpdater = self
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.dimsBackgroundDuringPresentation = false
    return searchController
  }()

  let collectionViewLayout = DashboardCollectionViewLayout()

  lazy var collectionView: UICollectionView = {
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
    collectionView.backgroundColor = .white
    collectionView.delegate = self
    collectionView.dataSource = dataSource
    collectionView.alwaysBounceVertical = true
    collectionView.register(
      DashboardComicCell.self, forCellWithReuseIdentifier: DashboardComicCell.cellIdentifier)
    return collectionView
  }()

  let loadingIndicator: UIActivityIndicatorView = {
    let view = UIActivityIndicatorView(style: .gray)
    return view
  }()

  override func viewDidLoad() {
    super.viewDidLoad()

    navigationItem.title = "XKCD ❤️"
    navigationItem.searchController = searchController

    [
      collectionView,
      loadingIndicator,
    ].forEach(view.addSubview(_:))

    collectionView.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }

    loadingIndicator.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }

    bindViewModel()

    viewModel.inputs.viewDidLoad()
  }

  func bindViewModel() {
    viewModel.outputs.comicIds
      .observe(on: UIScheduler())
      .observeValues { [weak self] comicIds in
        self?.dataSource.set(comicIds)
        self?.collectionView.reloadData()
    }

    viewModel.outputs.goToComic
      .observe(on: UIScheduler())
      .observeValues { [weak self] value in
        self?.goTo(currentComicId: value.current, comicIds: value.comicIds)
    }

    viewModel.outputs.isSearching
      .observe(on: UIScheduler())
      .observeValues { [weak self] isSearching in
        if isSearching {
          self?.loadingIndicator.startAnimating()
          self?.loadingIndicator.isHidden = false
        } else {
          self?.loadingIndicator.stopAnimating()
          self?.loadingIndicator.isHidden = true
        }
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.inputs.viewWillAppear(animated)
  }

  func goTo(currentComicId: Model.ComicId, comicIds: [Model.ComicId]) {
    let comicVC = ComicViewController.configuredWith(currentComicId: currentComicId, comicIds: comicIds)
    navigationController?.pushViewController(comicVC, animated: true)
  }
}

extension DashboardViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let value = dataSource[indexPath]

    switch value {
    case let value as Model.ComicId:
      viewModel.inputs.tappedOnComic(id: value)
    default:
      preconditionFailure()
    }
  }
}

extension DashboardViewController: UISearchControllerDelegate {
  func willDismissSearchController(_ searchController: UISearchController) {
    viewModel.inputs.searchCancelled()
  }
}

extension DashboardViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let text = searchController.searchBar.text ?? ""
    viewModel.inputs.search(term: text)
  }
}
