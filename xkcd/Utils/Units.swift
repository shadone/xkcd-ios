import Foundation

extension Int {
  var kilobytes: Int { return self * 1024 }
  var megabytes: Int { return kilobytes * 1024 }
}
