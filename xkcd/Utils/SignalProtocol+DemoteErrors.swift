import Foundation
import ReactiveSwift
import Result

extension SignalProtocol {
  func demoteErrors() -> Signal<Value, NoError> {
    return signal.flatMapError { _ in .empty }
  }
}

extension SignalProducer {
  func demoteErrors() -> SignalProducer<Value, NoError> {
    return lift { $0.demoteErrors() }
  }
}
