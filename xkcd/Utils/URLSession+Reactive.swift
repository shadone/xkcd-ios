import Foundation
import ReactiveSwift
import Result

extension URLSession {
  func fetchData(_ request: URLRequest) -> SignalProducer<Data, APIError> {
    let producer = reactive.data(with: request)

    return type(of: self).fetchData(request, producer: producer)
  }

  static func fetchData(
    _ request: URLRequest,
    producer: SignalProducer<(Data, URLResponse), AnyError>
    ) -> SignalProducer<Data, APIError> {
    return producer
      // TODO: for unit testing we need to be able to inject a scheduler:
      // .start(on: Application.current.networkScheduler)
      .flatMapError({ (error) -> SignalProducer<(Data, URLResponse), APIError> in
        let path: String
        if let url = request.url, let components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
          path = components.path
        } else {
          path = "??"
        }
        print("💀 \(path): Network error occurred: \(error)")
        return SignalProducer(error: .networkUnavailable)
      })
      .flatMap(.concat) { data, response -> SignalProducer<Data, APIError> in
        guard let response = response as? HTTPURLResponse else { fatalError() }

        let isSuccessResponse = (200 ..< 300).contains(response.statusCode)
        guard isSuccessResponse else {
          return SignalProducer(error: .badResponse(httpCode: response.statusCode))
        }

        return SignalProducer(value: data)
    }
  }
}
