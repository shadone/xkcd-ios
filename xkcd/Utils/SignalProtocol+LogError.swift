import Foundation
import ReactiveSwift

extension SignalProtocol {
  func logError(identifier: String = "") -> Signal<Value, Error> {
    return signal.mapError { error -> Error in
      print("\(identifier) error: \(error)")
      return error
    }
  }
}

extension SignalProducer {
  func logError(identifier: String = "") -> SignalProducer<Value, Error> {
    return lift { $0.logError(identifier: identifier) }
  }
}
