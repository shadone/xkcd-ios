// swiftlint:disable:next line_length
// https://github.com/kickstarter/Kickstarter-ReactiveExtensions/blob/60a8f0515ab0edb3316da190c390588a5ba2cfc8/ReactiveExtensions/operators/TakeWhen.swift
// License: Apache 2

import ReactiveSwift

public extension Signal {
  /**
   Emits the most recent value of `self` when `other` emits.
   - parameter other: Another signal.
   - returns: A new signal.
   */
  func takeWhen<U>(_ other: Signal<U, Error>) -> Signal<Value, Error> {
    return other.withLatestFrom(signal).map { tuple in tuple.1 }
  }
}
