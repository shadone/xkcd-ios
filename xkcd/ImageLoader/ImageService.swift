import Foundation
import ReactiveSwift
import Alamofire
import AlamofireImage
import enum Result.Result
import enum Result.NoError

class ImageService: ImageServiceType {
  private let urlCache: URLCache = {
    let cache = URLCache(memoryCapacity: 50.megabytes, diskCapacity: 200.megabytes, diskPath: "images")
    return cache
  }()

  private lazy var sessionManager: SessionManager = {
    let config = URLSessionConfiguration.default
    config.urlCache = urlCache

    let session = SessionManager(configuration: config)
    return session
  }()

  func get(_ url: URL) -> SignalProducer<UIImage, NoError> {
    return SignalProducer { [weak self] (observer, lifetime) in
      guard let strongSelf = self else {
        observer.sendInterrupted()
        return
      }

      strongSelf.sessionManager.request(url).responseImage { response in
        switch response.result {
        case let .failure(error):
          print("Failed to fetch image \(error)")
          // TODO: error handling?
          observer.sendCompleted()
        case let .success(image):
          observer.send(value: image)
          observer.sendCompleted()
        }
      }

    }
  }
}
