//
//  ImageServiceType.swift
//  xkcd
//
//  Created by Denis Dzyubenko on 23/06/2019.
//  Copyright © 2019 Denis Dzyubenko. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

protocol ImageServiceType {
  /// Fetches image from a given URL.
  ///
  /// TODO: implement caching
  ///
  /// - parameter url: image url
  func get(_ url: URL) -> SignalProducer<UIImage, NoError>
}
