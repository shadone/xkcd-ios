import UIKit

protocol ValueCell: AnyObject {
  associatedtype Value

  static var cellIdentifier: String { get }

  func configure(with value: Value)
}
