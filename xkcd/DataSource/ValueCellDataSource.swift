import UIKit

class ValueCellDataSource: NSObject, UICollectionViewDataSource {
  private var values: [(value: Any, reusableIdentifier: String)] = []

  func set<Cell: ValueCell>(_ values: [Any], cell: Cell.Type) {
    self.values = values.map { (value: $0, reusableIdentifier: cell.cellIdentifier) }
  }

  func set<Cell: ValueCell>(_ value: Any, cell: Cell.Type, row: Int) {
    self.values[row] = (value: value, reusableIdentifier: cell.cellIdentifier)
  }

  subscript(indexPath: IndexPath) -> Any {
    return self.values[indexPath.row].value
  }

  /// Meant to be re-implemented in subclasses
  func configure(cell: UICollectionViewCell, with value: Any) {
  }

  // MARK: UICollectionViewDataSource conformance

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return values.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let (value, reusableIdentifier) = values[indexPath.row]

    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusableIdentifier, for: indexPath)

    configure(cell: cell, with: value)

    return cell
  }
}
