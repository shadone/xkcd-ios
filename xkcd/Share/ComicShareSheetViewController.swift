import UIKit

class ComicShareSheetViewController: UIActivityViewController {
  static func configuredWith(comic: Model.Comic) -> ComicShareSheetViewController {
    let url = Application.current.apiService.shareUrl(for: comic.id)
    return ComicShareSheetViewController(activityItems: [url], applicationActivities: nil)
  }
}
