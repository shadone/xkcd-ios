import Foundation

struct AppConfiguration {
  let apiService: APIServiceType
  let imageService: ImageServiceType

  // MARK: Functions

  init(apiService: APIServiceType = APIService(),
       imageService: ImageServiceType = ImageService()) {
    self.apiService = apiService
    self.imageService = imageService
  }

  func clone(apiService: APIServiceType? = nil,
             imageService: ImageServiceType? = nil) -> AppConfiguration {
    return AppConfiguration(
      apiService: apiService ?? self.apiService,
      imageService: imageService ?? self.imageService)
  }

  static var `default` = AppConfiguration()
}
