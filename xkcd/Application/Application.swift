import Foundation

class Application {
  private static var stack: [AppConfiguration] = []
  static var current: AppConfiguration { return stack.last! }

  static func pushConfiguration(_ configuration: AppConfiguration) {
    stack.append(configuration)
  }

  static func popConfiguration() {
    _ = stack.popLast()
  }

  private init() {}
}
